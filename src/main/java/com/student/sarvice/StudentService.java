package com.student.sarvice;

import java.util.List;

import com.student.model.BSStudent;

public interface StudentService {
	
		public List<BSStudent> getAllData();
		
		public BSStudent getIdData(int id);
		public BSStudent saveStudent(BSStudent student);
		public void deleteStudent(int id);
		public void updateStudent(BSStudent student);
		
	
}

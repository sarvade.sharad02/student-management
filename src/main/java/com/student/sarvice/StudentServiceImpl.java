package com.student.sarvice;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.student.dao.StudentDao;
import com.student.model.BSStudent;

import lombok.extern.log4j.Log4j2;
@Service
@Log4j2
public  class StudentServiceImpl implements StudentService{
	Logger log = LoggerFactory.getLogger(StudentServiceImpl.class); 			
	
	List<BSStudent> slist;
	@Autowired
	StudentDao studentDao;
	@Override
	public List<BSStudent> getAllData() {
		slist = new ArrayList<BSStudent>();
		studentDao.findAll().forEach(std ->slist.add(std));
		return slist;
	}
	@Override
	public BSStudent getIdData(int id) {
		BSStudent student = studentDao.findById(id).get();
		return student;
	}
	@Override
	public BSStudent saveStudent(BSStudent student) {
		BSStudent saveStudent = studentDao.save(student);
		return saveStudent;
	}
	@Override
	public void deleteStudent(int id) {
		studentDao.deleteById(id);
	}
	
	// update data
	@Override
	public void updateStudent(BSStudent student) {

		studentDao.save(student);
	}
	

}

package com.student.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.student.model.BSStudent;
@Repository
public interface StudentDao extends CrudRepository<BSStudent, Integer>,
		PagingAndSortingRepository<BSStudent, Integer>{
	

}
